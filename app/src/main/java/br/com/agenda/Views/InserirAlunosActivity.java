package br.com.agenda.Views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import br.com.agenda.Dao.AlunoDAO;
import br.com.agenda.Models.Aluno;
import br.com.agenda.buildConfig.BuildConfig;
import br.com.agenda.buildConfig.R;

public class InserirAlunosActivity extends AppCompatActivity {

    private FormularioHelper helper ;
    public static final int CODIGO_FOTO = 567;
    String caminhoFoto = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inserir_alunos);
        helper = new FormularioHelper(this);

        Intent intent = getIntent();
        Aluno aluno = (Aluno) intent.getSerializableExtra("aluno");
        if(aluno != null){
            helper.preencheFormulario(aluno);
        }

        Button botaoFoto = findViewById(R.id.botao_foto);
        botaoFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                caminhoFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File foto = new File(caminhoFoto);
                Uri fotoURI = FileProvider.getUriForFile(InserirAlunosActivity.this,"br.com.agenda.Provider", foto);
                camera.putExtra(MediaStore.EXTRA_OUTPUT, fotoURI);
                startActivityForResult(camera, CODIGO_FOTO);
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == CODIGO_FOTO){
              helper.carregaImagem(caminhoFoto);
            }
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.layout.activity_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        /*Verifica qual foi o item selecionado no menu!*/
        switch(item.getItemId()){
            case R.id.menu_formulario_ok:
                Aluno aluno = helper.pegaAluno();
                AlunoDAO alunoDAO = new AlunoDAO(this);
                if(aluno.getId() == null){
                    alunoDAO.insereAlunos(aluno);
                }else{
                    alunoDAO.editar(aluno);
                }
                alunoDAO.close();
                Toast.makeText(InserirAlunosActivity.this,"aluno " +aluno.getNome() +" salvo", Toast.LENGTH_SHORT).show();
                finish();
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
