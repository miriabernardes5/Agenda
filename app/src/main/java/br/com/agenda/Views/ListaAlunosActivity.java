package br.com.agenda.Views;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import br.com.agenda.Dao.AlunoDAO;
import br.com.agenda.Models.Aluno;
import br.com.agenda.buildConfig.R;

public class ListaAlunosActivity extends AppCompatActivity {
    ListView listaAlunos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos);
        this.listaAlunos = findViewById(R.id.listaAlunos);
        listaAlunos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            /*Escuta o evento de click em um item da view, um item da lista por exemplo*/
            public void onItemClick(AdapterView<?> lista, View item, int position, long l) {
                Aluno aluno = (Aluno) listaAlunos.getItemAtPosition(position);
                Intent formInserirAluno = new Intent(ListaAlunosActivity.this, InserirAlunosActivity.class);
                formInserirAluno.putExtra("aluno", aluno);
                startActivity(formInserirAluno);
            }
        });

        Button addAlunos = findViewById(R.id.addAlunos);
        addAlunos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentFormulario = new Intent(ListaAlunosActivity.this,InserirAlunosActivity.class);
                startActivity(intentFormulario);
            }
        });

        registerForContextMenu(listaAlunos);
    }

    @Override
    protected void onResume() {
        super.onResume();
        CarregarLista();
    }

    private void CarregarLista() {
        AlunoDAO dao = new AlunoDAO(this);
        List<Aluno> alunos = dao.buscaAlunos();
        dao.close();


        AlunosAdapter adapter = new AlunosAdapter(this, alunos);
        this.listaAlunos.setAdapter(adapter);
    }

   @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
       AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
       final Aluno aluno = (Aluno) listaAlunos.getItemAtPosition(info.position);
       MenuItem deletar = menu.add("Deletar"); //atribui a referência da opção de menu criada.
       deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
           @Override
           public boolean onMenuItemClick(MenuItem item) {
               AlunoDAO dao = new AlunoDAO(ListaAlunosActivity.this);
               dao.deletar(aluno);
               dao.close();
               CarregarLista();
               return false;
           }
       });

       MenuItem siteItem = menu.add("Visitar site");

       String site = aluno.getSite();

       if(!site.startsWith("http://")){
            site =  "http://" + aluno.getSite();
       }
       Intent siteIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(site));
       siteItem.setIntent(siteIntent);


       MenuItem smsItem = menu.add("Enviar sms");
       Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + aluno.getTelefone()));
       smsItem.setIntent(smsIntent);

       MenuItem mapaItem = menu.add("Ver endereço");
       Intent mapaIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo: 0,0?q=" + aluno.getEndereco()));
       mapaItem.setIntent(mapaIntent);


       MenuItem ligarItem = menu.add("Ligar");

       ligarItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
           @Override
           public boolean onMenuItemClick(MenuItem item) {
                   if(ActivityCompat.checkSelfPermission(ListaAlunosActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                       ActivityCompat.requestPermissions(ListaAlunosActivity.this, new String[] {Manifest.permission.CALL_PHONE}, 123);
                   }else{
                       Intent intentLigar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + aluno.getTelefone()));
                       startActivity(intentLigar);
                   }
               return false;
           }
       });
   } }
